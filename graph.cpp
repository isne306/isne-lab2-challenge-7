#include"graph.h"
#include<list>
#include<string>
using namespace std;

int main() {

	int size;
	cout << "****** please input a graph. *****"<<endl;
	cout << "How many node : ";
	cin >> size;
	int **storegraph  = new int *[size];
	for (int i = 0; i < size; ++i) {
		storegraph [i] = new int[size];
	}
	cout << "****** Adjacency Matrix *****";
	cout << endl;
	cout << "   ";
	for (int i = 65; i < 65+ size; i++) {
		char ascii = i;
		cout << ascii << " ";
	}
	cout << endl;

	for (int i = 65; i < 65 + size; i++) {
		char ascii = i;

		cout << ascii << "  ";
		for (int j = 0; j < size; j++) {
			cin >> storegraph [i-65][j];
		}
	}
	cout << "****** Adjacency List*****"<<endl;
	Graphs g;
	g.adjacencylist(storegraph ,size);
	g.weights(size);

	cout << endl;
	cout << endl;
		cout << "******Check the graph*****"<< endl;
	cout << "Is it multigraph ?"<< endl;
	if (g.multiCheck())
		cout << "Yes"<< endl;
	else
		cout << "No"<< endl;
	cout << "Is it pseudograph ?"<< endl;
	if (g.pseudoCheck())
		cout << "Yes"<< endl;
	else
		cout << "No"<< endl;
	cout << "Is it digraph ?"<< endl;
	if (g.diCheck())
		cout << "Yes"<< endl;
	else
		cout << "No"<< endl;
	cout << "Is it weightedgraph ?"<< endl;
	if (g.weightCheck())
		cout << "Yes"<< endl;
	else
		cout << "No"<< endl;
	cout << "Is it completegraph ?"<< endl;
	if (g.completeCheck())
		cout << "Yes"<< endl;
	else
		cout << "No"<< endl;
	cout << endl;
	cout << endl;

	cout <<"******Shortest path******"<< endl;
	g.dijkstra(storegraph ,size);
;
}